
Maize Options
-------------

::

        # # #   Right Turn. 
        # + >   Buggy Detects Dead end. Buggy Detects Right Turn.  
        # ^ #   Buggy Go Right.

        # # #   T - Junction
        ? + >   Buggy Detects Dead end. Buggy Detects Right Turn.  
        # ^ #   Buggy Go Right.

        # ? #   T - Junction off to the Right.
        # + >   Buggy Detects Right Turn.  
        # ^ #   Buggy Go Right.

        # ? #   Cross road.
        ? + >   Buggy Detects Right Turn.  
        # ^ #   Buggy Go Right.

        # ^ #   T- Junction off to the Left
        ? + #   No Obstacle or Right turn detected.
        # ^ #   Buggy go straight.

        # # #   Dead end.
        # n #   Obstacle detected. No right turn. Turn left on the spot until 180 or left turn detected.
        # ^ #   Buggy go back.

        # # #   Left turn.
        < + #   Obstacle detected. No right turn. Turn left on the spot until 180 or left turn detected.
        # ^ #   Buggy go Left.

                End of Maize
                No Obstacle detected. Right Turn.
        # ^ #   Buggy go Right ;)

        # ^ #   Straight.
        # ^ #   No Obstacle detected, No Right Turn.
        # ^ #   Go Straight.


