/* 
 * File:   delay.h
 * Author: Martin Slabber
 *
 * Created June 2014
 */

/*
 *  Some generic delay functions.
 *  TODO: Calibrate !!!.
 */
#ifndef DELAY_H
#define	DELAY_H

#include "setup.h"

void delay(unsigned int sleep);
void delay_ms(unsigned int sleep);
void delay_us(unsigned int sleep);

#endif	/* DELAY_H */

