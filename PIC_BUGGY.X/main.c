/* PIGGY - Pic Buggy
 * A maze solver buggy using a dsPIC33.
 *
 * Created on September to November 2014
 * Authors: Rocco Kirsten and Martin Slabber
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <xc.h>
#include <pps.h>
// Include our drivers.
#include "delay.h"
#include "USART_driver.h"
#include "servo_motor.h"

_FOSCSEL(FNOSC_FRCPLL) // FNOSC_FRCPLL - Use internal Clock, upscale with PLL
_FWDT(FWDTEN_OFF) // Set the watchdog to off.

/* GLOBALS */
// enviro - Holds the currently known distances from the variouse sensors.
struct { // Create a struct and assign to variable.
    int distance_front; // in cm
    int distance_left;
    int distance_right;
    int power_left;
    int power_right;
    int power;
    int synced : 1; // True the Distance sensors are synced. False busy with sensor data aquasition.
    int direction; // Current dirction of travel.
    int move : 1; // True turn the wheels. Controlled by INT0.
    int bumper_left : 1;
    int bumper_right : 1;
} enviro;

// ir_reading - Infra Red reading values.

struct {
    int right_emit; // IR reading while emitting.
    int right_half; // IR reading while half emitting.
    int right_ambient; // IR reading while not emitting.
    int left_emit;
    int left_half;
    int left_ambient;
    int gap_counter;
} ir_reading;

// serial_rx_buffer - Holds the characters as recieved from the serial interface.

struct {
    char buffer[255];
    int ready : 1;
} serial_rx;

int step_counter = 0;

/* Functions */
void reset_gap_counter(void) {
    T3CONbits.TON = 0; // Stop the GAP counter.
    TMR3 = 0x00; // Clear timer register
    ir_reading.gap_counter = 0;
    T3CONbits.TON = 1; // Start the GAP counter.
}

void drift_right(void) {
    // Reduce power on the right so buggy drifts towards the right.
    enviro.power_left = enviro.power;
    if (enviro.power_right > 12000)
        enviro.power_right -= 10000;
    if (enviro.move) {
        P1DC1 = enviro.power_left;
        P1DC2 = enviro.power_right;
        P1TCONbits.PTEN = 1;
    }
}

void drift_left(void) {
    // Reduce power on the left so buggy drifts towards the left.
    enviro.power_right = enviro.power;
    if (enviro.power_left > 12000)
        enviro.power_left -= 10000;
    if (enviro.move) {
        P1DC1 = enviro.power_left;
        P1DC2 = enviro.power_right;
        P1TCONbits.PTEN = 1;
    }
}

void stop() {
    enviro.direction = STOP;
    RIGHT_FORWARD = 0;
    RIGHT_BACKWARD = 0;
    LEFT_FORWARD = 0;
    LEFT_BACKWARD = 0;
    PWM1CON1bits.PEN1H = 0; // PWM pin is enabled for PWM output
    PWM1CON1bits.PEN1L = 0; // PWM pin is disabled for PWM output
    PWM1CON1bits.PEN2H = 0; // PWM pin is enabled for PWM output
    PWM1CON1bits.PEN2L = 0; // PWM pin is disabled for PWM output
}

void move(int duration, int rf, int rb, int lf, int lb) {
    if (enviro.move) {
        P1DC1 = enviro.power_left;
        P1DC2 = enviro.power_right;
        P1TCONbits.PTEN = 1;
        // P2TCONbits.PTEN = 1;
        // Using Channel 1 and 2 on PWM 1.
        PWM1CON1bits.PEN1H = rb; // PWM pin is enabled for PWM output
        PWM1CON1bits.PEN1L = rf; // PWM pin is disabled for PWM output
        PWM1CON1bits.PEN2H = lb; // PWM pin is enabled for PWM output
        PWM1CON1bits.PEN2L = lf; // PWM pin is disabled for PWM output
    } else {
        stop();
    }
}

void forward(int duration) {
    T3CONbits.TON = 1;
    uart_put("{F}");
    enviro.direction = FORWARD;
    move(duration, 1, 0, 0, 1);
}

void backward(int duration) {
    uart_put("{B}");
    enviro.direction = BACKWARD;
    move(duration, 0, 1, 1, 0);
}

void left(int duration) {
    reset_gap_counter();
    uart_put("{L}");
    enviro.direction = LEFT;
    move(duration, 0, 1, 0, 1);
}

void right(int duration) {
    uart_put("{R}");
    enviro.direction = RIGHT;
    move(duration, 1, 0, 1, 0);
}

void soft_left() {
    reset_gap_counter();
    uart_put("{sL}");
    enviro.direction = LEFT;
    move(1, 0, 0, 0, 1);
}

void soft_right() {
    uart_put("{sR}");
    enviro.direction = RIGHT;
    move(1, 1, 0, 0, 0);
}

void soft_left_back() {
    reset_gap_counter();
    uart_put("{sbL}");
    enviro.direction = LEFT;
    move(1, 0, 1, 0, 0);
}

void soft_right_back() {
    uart_put("{sbR}");
    enviro.direction = RIGHT;
    move(1, 0, 0, 1, 0);
}

int calc_distance_from_ir(int emit, int half, int ambient) {
    /* Calculate a value for IR use; using ambient, half power emit and full
     * power emitting.
     *
     * Parameters
     * ----------
     * emit - Full power reading.
     * half - Half or reduced power reading,
     * ambient - Ambient reading.
     */
    int distance;
    int reading;
    reading = emit - ambient;
    distance = reading;
    return distance;
}

void update_distances() {

    // Check the BUMPERS.
    enviro.bumper_left = BUMPER_LEFT;
    enviro.bumper_right = BUMPER_RIGHT;
    // Take Ambient readings. These should be 0, but making sure.
    IR_TRIG = 0;
    IR_HALF_TRIG = 0;
    enviro.synced = 0;
    US_SENSOR_TRIG = 1;
    delay_us(10);
    US_SENSOR_TRIG = 0;

    delay_ms(100); //remove....
    // Right
    AD1CHS0 = 0; // select analog input channel based upon I/O epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.right_ambient = ADC1BUF0;
    // Left
    AD1CHS0 = 1; // select analog input channel based upon I/o epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.left_ambient = ADC1BUF0;

    IR_HALF_TRIG = 1; // Take readings while 1/2 emitting.
    delay_ms(20);

    // Left
    AD1CHS0 = 1; // select analog input channel based upon I/o epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.left_half = ADC1BUF0;
    // Right
    AD1CHS0 = 0; // select analog input channel based upon I/O epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.right_half = ADC1BUF0;

    IR_HALF_TRIG = 0;
    IR_TRIG = 1; // Take readings while emitting.
    delay_ms(20);

    // Left
    AD1CHS0 = 1; // select analog input channel based upon I/o epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.left_emit = ADC1BUF0;
    // Right
    AD1CHS0 = 0; // select analog input channel based upon I/O epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    ir_reading.right_emit = ADC1BUF0;


    IR_TRIG = 0;
    enviro.distance_right = calc_distance_from_ir(ir_reading.right_emit,
            ir_reading.right_half, ir_reading.right_ambient);
    enviro.distance_left = calc_distance_from_ir(ir_reading.left_emit,
            ir_reading.left_half, ir_reading.left_ambient);
    // Every time we check the distances, we have to decide to reset the GAP couneter.
    if (enviro.distance_left > LEFT_GAP_DETECT_DISTANCE)
        // No Gap Reset Counter.
        reset_gap_counter();
}

int get_front_distance() {
    int cntr;
    enviro.synced = 0;
    cntr = 0;
    while (!enviro.synced && cntr < 10) {
        US_SENSOR_TRIG = 1;
        delay_us(10);
        US_SENSOR_TRIG = 0;
        // Update_distances();
        uart_send("Wait for distance update.");
        delay_ms(30); // Trim here.
        cntr++;
    }
    return enviro.distance_front;
}

int side_distance_adjust() {
    // This function handles the data from the IR sensors.
    if (enviro.direction == FORWARD && (BUMPER_LEFT || BUMPER_RIGHT)) {
        // If we are in Forward mode. Side wall avoidance in only needed when
        // going forward, since we are not planning to ever go in reverse.
        if (BUMPER_LEFT && BUMPER_RIGHT) {
            reset_gap_counter();
            stop();
        } else {
            if (BUMPER_LEFT) {
                stop();
                uart_put("\r\n[bl]");
                while (BUMPER_LEFT) {
                    backward(1);
                    delay_us(60);
                }
                delay_ms(10);
                right(2);
                delay_ms(200);
                reset_gap_counter();
            } else if (BUMPER_RIGHT) {
                stop(); // Go with the uart_send.
                uart_put("\r\n[br]");
                while (BUMPER_RIGHT) {
                    backward(1);
                    delay_us(60);
                }
                delay_ms(10); // Go back for an additional 3ms.
                left(1);
                delay_ms(200);

            }
            forward(0);
        }
    }
    return 0;
}

void turn_90(int direction) {
    // Turn the buggy, about 90 degrees. But check the front as well.
    char buffer[255];
    int step_counter = 0;
    int front_clear = 0;

    // First make a swooping turn and only check bumpers.
    while (step_counter < 250) {
        // Do small turns with checks in between.
        step_counter++;
        if (direction == LEFT)
            left(1);
        else if (direction == RIGHT)
            right(1);
        else {
            // BAD The function was called incorectly.
            stop();
            step_counter = 1000;
            uart_send("turn 90 ERROR.!!!!");
            break;
        }
        delay_ms(10);
        if (BUMPER_LEFT) {
            while (BUMPER_LEFT)
                backward(1);
            stop();
        }
        if (BUMPER_RIGHT) {
            // TODO(MS): Not valid.
            while (BUMPER_RIGHT)
                backward(1);
            stop();
        }
        if (enviro.move == 0)
            // Recieved a stop signal.
            break;
        if (step_counter > 68 && (step_counter % 10) == 0) {
            // Last stage of the turn. Stop and take distance readings.
            stop();
            get_front_distance();
            if (enviro.distance_front > 15)
                front_clear++;
            else
                front_clear = 0;
            if (front_clear > 2) {
                // The last few times there was a big distance infront.
                step_counter = 1000;
                break;
            }
            sprintf(buffer, "Turn: SC:%d FC:%d D:%d", step_counter, front_clear, enviro.distance_front);
            uart_send(buffer);
        }
    }
    sprintf(buffer, "Turn done. sc: %d", step_counter);
    uart_send(buffer);
    reset_gap_counter();
}

void process_instruction(void) {
    // Process instructions from the serial interface.
    /* Seperate the recieved message into two tokens. */
    if (serial_rx.ready) {
        uart_send(serial_rx.buffer);

        char tok1[40];
        char tok2[40];
        char buffer[255];
        char *p = strchr(serial_rx.buffer, '=');
        if (p) {
            size_t len = p - serial_rx.buffer;
            strncpy(tok1, serial_rx.buffer, len);
            tok1[len] = '\0';
            p++;
            sprintf(tok2, "%s", p);
        } else {
            strncpy(tok1, serial_rx.buffer, 40);
            tok2[0] = '\0';
        }
        sprintf(buffer, "[%s - %s]\n\r", tok1, tok2);
        uart_send(buffer);
        switch (tok1[0]) {
            case 'g':
                enviro.move = 1;
                break;
            case 's':
                enviro.move = 0;
                stop();
                break;
            case 'l':
                enviro.move = 1;
                left(1);
                enviro.direction = LEFT;
                break;
            case 'f':
                enviro.move = 1;
                enviro.direction = FORWARD;
                forward(1);
                break;
            case 'r':
                enviro.move = 1;
                enviro.direction = RIGHT;
                right(1);
                break;
            case 'b':
                enviro.move = 1;
                enviro.direction = BACKWARD;
                backward(1);
                break;
            case 't':
                enviro.move = 1;
                turn_90(LEFT);
                stop();
                enviro.move = 0;
                break;
        }
        serial_rx.ready = 0;
        serial_rx.buffer[0] = '\0';
    }
}

int information_display_updater(int loop) {
    // Send status information to the serial interface.
    char buffer[255]; // General purpose buffer to write to USB with.
    if (loop > 30) { // Just for information sake.
        sprintf(buffer, "%4d Dir: %d |Dist: Frnt %2d Lft %3d Rght %3d",
                step_counter,
                enviro.direction,
                enviro.distance_front,
                enviro.distance_left,
                enviro.distance_right
                );
        uart_put(buffer);
        sprintf(buffer, " |IR: Lft %3d %3d %3d Rght %3d %3d %3d",
                ir_reading.left_emit,
                ir_reading.left_half,
                ir_reading.left_ambient,
                ir_reading.right_emit,
                ir_reading.right_half,
                ir_reading.right_ambient);
        uart_put(buffer);
        sprintf(buffer, " GAP %3d",
                ir_reading.gap_counter);
        uart_put(buffer);
        sprintf(buffer, " |Bumper: L: %2d R: %2d",
                enviro.bumper_left,
                enviro.bumper_right);
        uart_send(buffer);
        step_counter += 1;
        if (step_counter > 10000000)
            // should not end up in the final product this is a test.
            step_counter = 11;
        loop = 0;
    } else
        loop++;
    return loop;
}

void follow_detour(void) {
    // Average the last few values on the ring buffer to see if this is a big GAP.
    update_distances();
    if (enviro.distance_left < LEFT_GAP_DETECT_DISTANCE) {
        uart_send("follow_detour.?");
        turn_90(LEFT);
    } else {
        uart_send("Na that gap did not have a nice avg..");

    }
    stop();
}

void dead_end_escape(void) {
    // We are standing still but the system wants to move.
    if (enviro.move && enviro.direction == STOP) {
        uart_send("dead_end escape.!");
        update_distances(); // Update the distance values from the sensors.
        information_display_updater(100);
        if (enviro.distance_front > 5) {
            forward(1);
        } else {
            // Check the side distances.
            if (enviro.distance_right < RIGHT_GAP_DETECT_DISTANCE && enviro.distance_left > LEFT_GAP_DETECT_DISTANCE) {
                // There is a GAP to the Right and no GAP to the Left.
                turn_90(RIGHT);
            } else {
                // When all else failes go Left.
                turn_90(LEFT);
            }
            forward(1);
        }
    } else {
        uart_send("!! NOT Sure how we ended up here in dead_end_ecape.!!!");
    }
}

void setup(void) {
    // Set global values.
    enviro.move = 0;
    enviro.distance_front = 20;
    enviro.distance_left = 20;
    enviro.distance_right = 20;
    enviro.direction = FORWARD;
    enviro.power_left = POWER;
    enviro.power_right = POWER;
    enviro.power = POWER;
    enviro.bumper_left = 0;
    enviro.bumper_right = 0;
    ir_reading.gap_counter = 0;
    serial_rx.buffer[0] = '\0';
    serial_rx.ready = 0;
    AD1PCFGL = 0xFFFF; // Change AN pins to digital.
    AD1CON1bits.ADON = 0; // Disable ADC
    /* Setup the various components.*/
    setup_clk();
    setup_physical_pins();
    setup_uart();
    setup_timer1();
    setup_timer3();
    setup_ultrasonic();
    setup_adc();
    setup_pwm();
    //side_ring_buffer_flush();
}

int main(void) {
    setup();
    // Assume we have free space arround the buggy when we start.
    uart_send("Warm up.!!");
    int loop = 0;
    int cntr;
    enviro.move = 1;
    reset_gap_counter();
    while (1) {
        cntr = 0;
        while (!enviro.synced && cntr < 10) {
            // Create a delay here until distance is synced.
            // This is to insure we do not over run the US sensor.
            // Consider a stop here as well.
            uart_put("z"); // Having sone zzzzzzzz's
            delay_ms(50);
            cntr++;
        }
        update_distances(); // Update the distance values from the sensors.
        loop = information_display_updater(loop); // After a few loops print some info.
        process_instruction(); // See if there are commands on the serial RX buffer that needs to be processed.
        switch (enviro.direction) {
                // Buggy is currently performing one of the following actions.
            case FORWARD:
                if (enviro.distance_front < 6)
                    // To close to a wall in the front.
                    stop();
                else {
                    if (ir_reading.gap_counter > GAP_DETECTION_COUNTER) {
                        stop();
                        follow_detour();
                    } else {

                        side_distance_adjust(); // We are going forward. Check that we do not hit the wall.
                    }

                }
                break;
            case LEFT:
                if (enviro.distance_left > LEFT_SAVE_DISTANCE_FROM_WALL)
                    stop();
                break;
            case RIGHT:
                if (enviro.distance_right > RIGHT_SAVE_DISTANCE_FROM_WALL)
                    stop();
                break;
            case BACKWARD:
                // Only used when we manualy control buggy via serial.
                backward(0);
                break;
            case STOP:
                if (enviro.move) {
                    // TODO: When enviro.move=1 and enviro.direction=STOP do something to get moving again.
                    // call a rotate/search method. this is also where whe get out of dead end
                    // and could be the starting point for maze solving.
                    if (ir_reading.gap_counter > GAP_DETECTION_COUNTER) {
                        follow_detour();
                    } else {
                        dead_end_escape();
                    }
                } else
                    delay_ms(100);
                break;
            default:
                enviro.direction = FORWARD;
        }
    }
}

void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void) {
    /* Interrupt Service Routine code goes here */
    if (enviro.direction == FORWARD) {
        ir_reading.gap_counter++;
        if (ir_reading.gap_counter > GAP_DETECTION_COUNTER)
            stop();
    }
    IFS0bits.T3IF = 0; // Clear Timer3 Interrupt Flag
}

void __attribute__((__interrupt__, auto_psv)) _CNInterrupt(void) {
    // Change notification Interupt. Used on the ultrasonic sensor.
    int distance;
    if (US_SENSOR_ECHO) {
        // PIN HIGH - start of distance signal
        TMR1 = 0;
        T1CONbits.TON = 1; // Start Timer
        // Note to self. This interup fires in close succession, dont put any code here.
    } else {
        // PIN LOW  - end of distance signal.
        T1CONbits.TON = 0; // Stop Timer
        distance = TMR1;
        enviro.distance_front = distance / 36; // Normalise to cm.
        // TODO(MS): Do something to insure it is not negative.
        enviro.synced = 1;
    }
    IFS1bits.CNIF = 0; // Clear CN interrupt
}

void __attribute__((__interrupt__, auto_psv)) _U1TXInterrupt(void) {
    // UART1 Tx interupt
    IFS0bits.U1TXIF = 0;
}

void __attribute__((__interrupt__, auto_psv)) _U1RXInterrupt(void) {
    // UART1 Rx interupt
    // It is normaly bad practive to have so much code in a ISR, but this one
    // handles user input and it is ok to disrupt the rest of the program flow.
    char buffer[255];
    char ch = 'x';
    if (U1STAbits.FERR == 1) {
        //    continue;
        Nop();
    }

    while (U1STAbits.URXDA == 1) {
        ch = U1RXREG;
        sprintf(buffer, "{%d}", (int) ch);
        uart_send(buffer);
        switch (ch) {
            case 13:
                serial_rx.ready = 1;
                break;
            case 32: // Space is stop.
                enviro.move = 0;
                serial_rx.ready = 1;
                stop();
                sprintf(serial_rx.buffer, "s");
                enviro.power_right = POWER;
                enviro.power_left = POWER;
                break;
            case 27: // Special Keys.
                // Handle Arrow keys.
                ch = U1RXREG;
                if (ch == 91) {
                    // Strict checking of every bite.
                    ch = U1RXREG;
                    switch (ch) {
                        case 65: // ^
                            ch = 'f';
                            break;
                        case 66: // v
                            ch = 'b';
                            break;
                        case 67: // >
                            ch = 'r';
                            break;
                        case 68: // <
                            ch = 'l';
                            break;
                    }
                    serial_rx.ready = 1;
                    sprintf(serial_rx.buffer, "%s%c", serial_rx.buffer, ch);
                }
                break;
            case '>':
            case '.':
                // pull to the right
                //drift_right();
                soft_right();
                break;
            case '<':
            case ',':
                // pull to the left.
                //drift_left();
                soft_left();
                break;
            case '+':
            case '=':
                // Speed up
                enviro.power += 1000;
                sprintf(buffer, "+Power: %d", enviro.power);
                uart_send(buffer);
                break;
            case '-':
            case '_':
                // Slow down.
                enviro.power -= 1000;
                sprintf(buffer, "-Power: %d", enviro.power);
                uart_send(buffer);
                break;
            case '?':
            case '/':
                // Print Information.
                information_display_updater(100);
                sprintf(buffer, "Power: %d", enviro.power);
                uart_send(buffer);
                sprintf(buffer, " Left:%d", enviro.power_left);
                uart_send(buffer);
                sprintf(buffer, " Right:%d", enviro.power_right);
                uart_send(buffer);
                break;
            default:
                sprintf(serial_rx.buffer, "%s%c", serial_rx.buffer, ch);
        }
    }

    if (U1STAbits.OERR == 1) {
        U1STAbits.OERR = 0;
        //continue;
    }
    IFS0bits.U1RXIF = 0;
}

void __attribute__((__interrupt__, auto_psv)) _INT0Interrupt(void) {
    // Run/Go button.
    enviro.move ^= 1;
    IFS0bits.INT0IF = 0; // clear int0 interrupt flag
}

void __attribute__((__interrupt__, auto_psv)) _INT1Interrupt(void) {
    // Right bumper interupt.
    IFS1bits.INT1IF = 0; // clear int1 interrupt flag
}

void __attribute__((__interrupt__, auto_psv)) _INT2Interrupt(void) {
    // Left bumper interupt.
    IFS1bits.INT2IF = 0; // clear int2 interrupt flag
}