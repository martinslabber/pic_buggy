#include <xc.h>
#include "setup.h"
#include "servo_motor.h"
#include "delay.h"



void stop() {
    // Put both motors in stop.
    P1DC1 = RightMotor.stop;
    P1DC2 = LeftMotor.stop;
    P1TCONbits.PTEN = 1;
    P2TCONbits.PTEN = 1;
}

void move(int time) {
    delay_ms(time);
    stop();
}

void go_forward(int time) {
    // Go forward. Put both motors in forward.
    P1DC1 = RightMotor.forward;
    P1DC2 = LeftMotor.forward;
    P1TCONbits.PTEN = 1;
    P2TCONbits.PTEN = 1;
    move(time);
}

void go_backward(int time) {
    // Put both motors in backwards.
    P1DC1 = RightMotor.backward;
    P1DC2 = LeftMotor.backward;
    P1TCONbits.PTEN = 1;
    P2TCONbits.PTEN = 1;
    move(time);
}

void go_left(int time) {
    // Turn Left put
    P1DC1 = RightMotor.forward;
    P1DC2 = LeftMotor.backward;
    P1TCONbits.PTEN = 1;
    P2TCONbits.PTEN = 1;
    move(time);
}

void go_right(int time) {
    P1DC2 = LeftMotor.forward;
    P1DC1 = RightMotor.backward;
    P1TCONbits.PTEN = 1;
    P2TCONbits.PTEN = 1;
    move(time);
}