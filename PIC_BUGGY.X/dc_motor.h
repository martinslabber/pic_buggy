/* 
 * File:   dc_motor.h
 * Author: martin slabber
 *
 * Created on October 8, 2014, 9:23 PM
 */

#ifndef MOTOR_H
#define	MOTOR_H

void stop();
void go_forward(int time);
void go_backward(int time);
void go_left(int time);
void go_right(int time);

#endif	/* DC_MOTOR_H */

