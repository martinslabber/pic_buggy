/* 
 * File:   setup.h
 *
 */

/* Global setup of the Chip. Please include this file first.
 */
#ifndef SETUP_H
#define	SETUP_H

#include <xc.h>

// See init_clk() in main.c on how Fosc is determined.
#define FCY        40000000     // Set Frequency of instructions: FCY = Fosc/2

#define POWER 30000

// If the Left and Right sensors get readings above this value. It is to close to the wall.
#define RIGHT_SAVE_DISTANCE_FROM_WALL 450
#define LEFT_SAVE_DISTANCE_FROM_WALL 450
// If the Right hand sensor get a value less than this it reads it as a Gap.
#define RIGHT_GAP_DETECT_DISTANCE 60
#define LEFT_GAP_DETECT_DISTANCE 60

#define GAP_DETECTION_COUNTER 19 // After this many polls and the GAP was there, stop & turn.

#define STOP 0
#define LEFT 1
#define RIGHT 2
#define FORWARD 3
#define BACKWARD 4

/* PINout */
// PIN 1 - -MCLR [Do not use]
// PIN 2
//  ADC - Right IR Echo
// PIN 3
//  ADC - Left IR Echo
// PIN 4
//  Programer PGEDA [Do not use]
// PIN 5
//  Programer PGECA [Do not use]
// PIN 6
//  Right IR Trigger
#define IR_HALF_TRIG	_LATB2
#define IR_HALF_TRIG_TRIS	_TRISB2
// PIN 7
//  Left IR Trigger
#define IR_TRIG	_LATB3
#define IR_TRIG_TRIS	_TRISB3
// PIN 8
//  GND [Do not use]
// PIN 9
//
// PIN 10
//
// PIN 11
//
#define BUMPER_LEFT _RB4
#define BUMPER_LEFT_TRIS _TRISB4
// PIN 12
//
#define BUMPER_RIGHT _RA4
#define BUMPER_RIGHT_TRIS _TRISA4
// PIN 13
//  Power [Do not use]
// PIN 14
// 
//-----------------
// PIN 15
//
// PIN 16 (INT0)
//  ? Could be used for external interups, maybe a kill switch.
// RG = Run/GO
#define RG_BUTTON	    _RB7
#define RG_BUTTON_TRIS	    _TRISB7
// PIN 17
//  Ultrasonic Triger Pin - Yellow wire.
#define US_SENSOR_TRIG      _LATB8
#define US_SENSOR_TRIG_TRIS _TRISB8
// PIN 18
//  Ultrasonic Echo Pin - Green wire.
#define US_SENSOR_ECHO      _RB9
#define US_SENSOR_ECHO_TRIS _TRISB9
#define US_CN_PIN           CNEN2bits.CN21IE
#define US_CN_PULLUP        CNPU2bits.CN21PUE
// PIN 19
//  GND [Do not use]
// PIN 20
//  Vcap [Do not use]
// PIN 21
//  UART tX - setup_physical_pins in main.
// PIN 22
//  UART rX - setup_physical_pins in main.
// PIN 23
#define LEFT_BACKWARD  _LATB12
#define LEFT_BACKWARD_TRIS  _TRISB12
// PIN 24
#define LEFT_FORWARD  _LATB13
#define LEFT_FORWARD_TRIS  _TRISB13
// PIN 25
#define RIGHT_BACKWARD  _LATB14
#define RIGHT_BACKWARD_TRIS  _TRISB14
// PIN 26
#define RIGHT_FORWARD  _LATB15
#define RIGHT_FORWARD_TRIS  _TRISB15
// PIN 27
//  GND [Do not use]
// PIN 28
//  Power [Do not use]
//
//-----------------
//
// SPI MISO
// SPI SIMO
// SPI CLK
// SPI EN/CS
//
// Optical Input
//

// TYPES
void setup_clk(void);
void setup_physical_pins(void);
void setup_timer1(void);
void setup_timer3(void);
void setup_ultrasonic(void);
void setup_adc(void);
void setup_pwm(void);

#endif	/* SETUP_H */

