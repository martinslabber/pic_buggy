#include "setup.h"
#include "delay.h"
// Accademic excerside on accurate timing for the PIC.
// 1 - Surely some one has done this before and there should be a library out there. Should check but this is more fun.
// 2 - Not validated with osciliscope or even a watch. Thus Author is blisfully unaware of inacuracy.

#define __DELAY_MS_FCY (FCY / 13000) // Amount of time (ms) each clock cycle takes.

void delay_ms(unsigned int sleep) {
    // This delay will sleep for the amount of milli seconds.
    // See how many clock cycles is required to make up sleep milliseconds
    // unsigned int cycles = sleep / __DELAY_MS_FCY;
    unsigned long cycles = sleep * __DELAY_MS_FCY;
    unsigned long n;
    for (n = 0; n < cycles; n++);
}

#define __DELAY_US_FCY (FCY / 13000000) // Amount of time (ms) each clock cycle takes.

void delay_us(unsigned int sleep) {
    // This delay will sleep for the amount of milli seconds.
    // See how many clock cycles is required to make up sleep milliseconds
    unsigned long cycles = sleep * __DELAY_US_FCY;
    unsigned long n;
    for (n = 0; n < cycles; n++);
}

void delay(unsigned int sleep) {
    int n;
    for (n = 0; n < sleep; n++) {
        delay_ms(997); // TODO: Adjust this on osciliscope.
    }
}