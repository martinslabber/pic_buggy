#include <xc.h>

void setup_ADC(void) { //recheck bit settings for 12 bits
    AD1PCFGL = 0xFFFD; // select analog input pins
    AD1CON1 = 0x00E0; // auto convert after end of sampling
    AD1CON1bits.AD12B = 1; //set to 12 bits
    AD1CSSL = 0; // no scanning required
    AD1CON3 = 0x1F02; // system clock,max sample time = 31Tad, Tad = 2 x Tcy = 125ns >75ns
    AD1CON2 = 0; // use MUXA, AVss and AVdd are used as Vref+/-
    AD1CON1bits.ADON = 1; // turn on the ADC
} // setup_ADC

int read_ADC(void) {
    AD1CHS0 = 1; // select analog input channel based upon I/o epxansion pin
    AD1CON1bits.SAMP = 1; // start sampling, automatic conversion will follow
    while (!AD1CON1bits.DONE); // wait to complete the conversion
    return ADC1BUF0; // read the conversion result
} // read_ADC
