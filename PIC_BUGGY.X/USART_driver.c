/* Utility functions to use the serial data interface on the PIC.
 * The built in uart.c library could be used. I prefer the explicit definition.
 */
#include <xc.h>
#include "setup.h"
#include "USART_driver.h"

void setup_uart(void) {
    /* setup serial interface, Via USB level converter.*/
    U1MODEbits.STSEL = 0; // 0= 1 stop bit.
    U1MODEbits.PDSEL = 0; // 8N  8bits no parity
    U1MODEbits.ABAUD = 0; //
    U1MODEbits.BRGH = 0; // Low speed
    U1MODEbits.LPBACK = 0; // 0=No loopback.
    U1BRG = BRGVAL;

    // TX interupt.
    U1STAbits.UTXISEL0 = 0;
    U1STAbits.UTXISEL1 = 0;
    U1STAbits.URXISEL0 = 0;
    U1STAbits.URXISEL1 = 0;
    IPC2bits.U1RXIP = 6; // Set the priority on the RX interupt.

    IFS0bits.U1TXIF = 0; // clear TX interrupt flag
    IEC0bits.U1TXIE = 0; // 1=Enable TX interupt.
    IFS0bits.U1RXIF = 0; // clear RX interrupt flag
    IEC0bits.U1RXIE = 1; // 1=Enable RX interupt.

    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART TX
    // Clear RX errors.
    U1STAbits.OERR = 0;
    U1STAbits.FERR = 0;
    U1STAbits.PERR = 0;

}

void uart_put(const char * ch) { // Pointer to the character array.
    // Send a character to the serial interface.
    while (*ch) {// For each charecter.
        while (_TRMT == 0); //If the transmit Shift register is not empty.
        U1TXREG = *ch++; // Send a character.
    }
    // Line feed at the end of each message.
}
void uart_send(const char * ch) { // Pointer to the character array.
    uart_put(ch);
    U1TXREG = '\n';
    U1TXREG = '\r';
}

char uart_get_char(void) {
    char ch = 0;
    /* check for receive errors */
    while (ch == 0) {
        if (U1STAbits.FERR == 1) {
            continue;
        }

        if (U1STAbits.OERR == 1) {
            U1STAbits.OERR = 0;
            continue;
        }

        if (U1STAbits.URXDA == 1) {
            ch = U1RXREG;
        }
    }
    return ch;
}