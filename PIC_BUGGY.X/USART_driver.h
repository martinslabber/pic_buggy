/* 
 * File:   USART_driver.h
 */

#ifndef USART_DRIVER_H
#define	USART_DRIVER_H


#define BAUDRATE   19200.0// USART baudrate.
#define BRGVAL      ((FCY / BAUDRATE) / 16.0) - 1.0

void setup_uart(void);

void uart_send(const char * ch);
void uart_put(const char * ch);
char uart_get_char();

#endif	/* USART_DRIVER_H */

