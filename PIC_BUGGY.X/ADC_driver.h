/* 
 * File:   ADC_driver.h
 * Author: martinslabber
 *
 * Created on June 14, 2014, 2:43 PM
 */

#ifndef ADC_DRIVER_H
#define	ADC_DRIVER_H

#define ADC_JITTER = 15; // ADC must change by this much.

void setup_ADC(void);
int read_ADC(void);

#endif	/* ADC_DRIVER_H */

