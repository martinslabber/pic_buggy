
.. note:: Pin numbers need to be double checked, was late when I counted pins.

Motors
======

Motors are connected to a J2930NF H-Bridge IC.

Left Motor to pin 11 & 14 of H-Bridge
Right Motor to pin 3 & 6 of H-Bridge

H-Bridge to PIC
* 2 - 25
* 7 - 26
* 10 - 24
* 15 - 23

H-bridge to power and ground
+5V: 1, 8, 9, 16
GND: 4, 5, 12, 13

Ultrasonic Sensor
=================

HC-SR04 is used and is mounted low on the front of the buggy.

The sensor has 4 pins connected as follow:
# Vcc - Red - +5V
# Trig - Yellow - PIC pin 17
# Echo - Green - PIC pin 18
# GND - Black - GND

Serial Communications
=====================

USB serial converter is connected to PIC.
The PIC UART is setup with pin 21 as RX and pin 22 as TX.

Programmer and PIC
==================

The programmer is connected to pin 1, 2, 5 of the PIC as well as +3V3 and GND.

Capacitors is added to the PIC as described in the datasheet.

A diode (and not a 10K Ohm resistor) is used from MCLR to the RC circuit. 
This circuit is described in the datasheet. 
The diode makes it possible to add or remove the programmer with out needing to adjust the circuit. 

Voltage Regulator
=================

+5V
---

7805 is used to regulate the battery voltage to +5V.

::

    HHHHH   AS seen from the Front.
    HH HH   
    HHHHH   I - Input to battery +
    #####   G - Ground to battery - and GND
    #####   O - Output to +5V5
    | | |  
    I G O

+3V3
----

10Ohm Resistor is connected between +5V and +3V3
A forward diode and a reversed 2V7 Zener are placed between +3V3 and GND.
A Large 1000uF (To big but its what I had) is placed between +3V3 and GND as a tank capacitor to absorb power fluctuations.

IR Sensor
=========

IR Components
* L-51P3BT : Transistor (Blue) 
* L-53F3BT : Diode (Blue)
* L-53F3C: Diode (Clear)

Voltage: 3.3V
The Emitter of 51P3 is ECHO
51P3 bias resistors: Vcc to Collector 1KOhm, Gnd to Emitter ~5KOhm.
53P diodes each has a 330Ohm resistor to GND.
2N2222: Base is Trigger, Collector to +3V3, Emitter to anodes of 53P diodes.


